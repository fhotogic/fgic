#! python
import classes
import cgi
import cgitb
cgitb.enable()
print ("Content-Type: text/html\n")

# Read the submitted form.
req_form = cgi.FieldStorage()
newUser = classes.User(req_form.getvalue('fname'), req_form.getvalue('lname'), req_form.getvalue('email'), req_form.getvalue('password'))

connection = classes.DBaccess()
cursor = connection.createConn()
if ( connection.addUser(cursor, newUser)  == 1):
	print ("SUCCESSFUL")
else:
	print ("FAILED")

