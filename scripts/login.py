#! python
import classes
import cgi
import cgitb
cgitb.enable()
print ("Content-Type: text/html\n")

# Read the submitted form.
req_form = cgi.FieldStorage()
loginUser = classes.User('N/A', 'N/A', req_form.getvalue('username'), req_form.getvalue('password'))

connection = classes.DBaccess()
cursor = connection.createConn()
if ( connection.validateUser(cursor, loginUser) ):
	print ("LOGIN SUCCESSFUL")
else:
	print ("LOGIN FAILED")

