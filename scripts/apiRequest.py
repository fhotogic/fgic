import classes
import json
import urllib.parse
import urllib.request
import oauth2 as oauth

# Google requst accepts a address string and convert it to latitude and longitude
# We are calling google map api to obtain latitude and longitude.		
class GoogleRequest:
	def __init__(self, address = 0):
		self.googleURL = 'https://maps.googleapis.com/maps/api/geocode/json'
		self.adrs = address
	
	# this is the function that covers address into 
	# latitude and longitude and returns a object of type Location
	def tryGeocode(self, address):
		self.adrs = {'address' : address }
		self.httpData = (urllib.parse.urlencode(self.adrs))
		self.googleReq = self.googleURL + '?' + self.httpData
		self.googleResp = urllib.request.urlopen(self.googleReq)
		self.jsonResp = json.loads(self.googleResp.read().decode(self.googleResp.info().get_param('charset') or 'utf-8'))
		self.loc = classes.Location(self.jsonResp['results'][0]['geometry']['location']['lat'], self.jsonResp['results'][0]['geometry']['location']['lng'], self.adrs, self.jsonResp['results'][0]['formatted_address'])
		return self.loc
		

# Twitter Request class. This will handle the website authentication
# with twitter api and also retrieve data from twitter
class TwitterRequest:
	twitterURL = 'https://api.twitter.com/1.1/search/tweets.json'
	CONSUMER_KEY = 'yBfPCvOpMJ4HsyiUM7yhEC9Pv'
	CONSUMER_SECRET = 'VC9am2XoRLGJdp9RTPMPQjJdPjAcAowQrqrOJcLT8EqGwM6lmT'
	ACCESS_KEY = '3234195049-5nMaeZjUvLUahTzL3i6PLLcjhTQo8i1JvpKZeg1'
	ACCESS_SECRET = 'TVYex59SrJfbBemJRZbPDIMkl1K02EkhQc2cHeOExyQGp'
	consumer = oauth.Consumer(key=CONSUMER_KEY, secret=CONSUMER_SECRET)
	access_token = oauth.Token(key=ACCESS_KEY, secret=ACCESS_SECRET)
	client = oauth.Client(consumer, access_token)
	
	def __init__(self):
		self.twitArgs = 0
		self.twitArg = 0
		self.twitReqWithArgs = 0
		
	def makeRequest(self, lat, long, dist, count, tweet_type = 'none'):
		self.twitArgs = {'geocode' : str(lat) + ',' + str(long) + ',' + dist + 'km', 'result_type' : 'recent', 'count' : count}
		self.twitArg = (urllib.parse.urlencode(self.twitArgs))
		#self.twitReqWithArgs = TwitterRequest.twitterURL + '?q%3DFilter:' + tweet_type +'%26' + self.twitArg
		self.twitReqWithArgs = TwitterRequest.twitterURL + '?q=filter%3A' + tweet_type +'&' + self.twitArg
		self.response, self.data = TwitterRequest.client.request(self.twitReqWithArgs)
		self.twitterResp = json.loads(self.data.decode( 'utf-8'))
		return self.twitterResp
		
		
# Instagram Request class. This will handle the website authentication
# with Instagram api and also retrieve data from Instagram
class InstagramRequest:
	InstagramURL = 'https://api.instagram.com/v1/media/search'
	ACCESS_TOKEN = '1905738915.bf0d810.68ce85b9345a45c08d049191fdb44527'
	
	def __init__(self):
		self.instaArgs = 0
		self.instaArg = 0
		self.maxcount = 0
		
	def makeRequest( self, lat, long, dist, maxcount):
		self.instaArgs = {'lat' : lat, 'lng' : long , 'distance' : dist, 'access_token' : InstagramRequest.ACCESS_TOKEN, 'count' : maxcount}
		#print (self.instaArgs)
		self.instaArg = (urllib.parse.urlencode(self.instaArgs))
		self.InstagramReq = InstagramRequest.InstagramURL + '?' + self.instaArg
		self.InstagramResp = urllib.request.urlopen(self.InstagramReq)
		self.InstaReturn = json.loads(self.InstagramResp.read().decode(self.InstagramResp.info().get_param('charset') or 'utf-8'))
		return self.InstaReturn
		
		