#! python
import classes
import apiRequest
import cgi
import cgitb
cgitb.enable()
print ("Content-Type: text/html\n")
print ('<!DOCTYPE HTML>')
print ('<html> <head>')
print ('	<meta http-equiv="Content-Type" content="text/html" charset="UTF-8"  />')
print ('	<title>Fhotogic</title>')
print ('	<link rel="stylesheet" href="../main.css">')
print ('</head> <body> <p>')


# Read the submitted form.
req_form = cgi.FieldStorage()

# Get the location entered by user in the search fielld.
form_loc =  req_form.getvalue('entered_value')

# Make a google api request and recieve back a Location object 
myGoogleReq = apiRequest.GoogleRequest()
myLocation = myGoogleReq.tryGeocode(form_loc)

# Extract the options selected by user on the homepage ( Checkboxes and distance )
if req_form.getvalue('distance'):
	distance = req_form.getvalue('distance')
	distanceKM = str(int(distance)/1000)
else:
	distance = '2000'
	distanceKM = '2'
	
if req_form.getvalue('maxcount'):
	maxcount = req_form.getvalue('maxcount')
else:
	maxcount = '50'

#if FACEBOOK is selected
if req_form.getvalue('facebook_option'):
	print ('FACEBOOK NOT SUPPORTED YET')
	

#if INSTAGRAM is selected	
if req_form.getvalue('instagram_option'):
	myInstagramReq = apiRequest.InstagramRequest()
	instagram_resp = myInstagramReq.makeRequest(myLocation.lat, myLocation.long, distance, maxcount)
	if req_form.getvalue('pictures_option'):
		for posts in instagram_resp['data']:
			picture = classes.Picture()
			picture.setPicInfo('standard_resolution', (posts['images']['standard_resolution']['url'])[-3:], posts['images']['standard_resolution']['url'], myLocation)
			i_htmP = '<img src="{pic}">'.format(pic = picture.name)
			print(i_htmP)
			
	if req_form.getvalue('videos_option'):
		for posts in instagram_resp['data']:
			if 'videos' not in posts:
				continue
			else:
				video = classes.Video()
				video.setVideoInfo('default', 'mp4', posts['videos']['low_bandwidth']['url'], myLocation)
				i_htmV = '<video controls> <source src="{vid}" type="video/mp4"> </video>'.format(vid = video.name)
				print(i_htmV)
	
	
#if TWITTER is selected	
if req_form.getvalue('twitter_option') :
	myTwitterReq = apiRequest.TwitterRequest()
	if req_form.getvalue('pictures_option'):
		twitter_resp = myTwitterReq.makeRequest(myLocation.lat, myLocation.long, distanceKM, maxcount, 'images')
		#print ( apiRequest.json.dumps(twitter_resp, indent=4, sort_keys=True))
		for tweets in twitter_resp['statuses']:
			if 'media' not in tweets['entities']:
				continue
			else:
				for tweet in tweets['entities']['media']:
					if 'media_url' not in tweet:
						continue
					else:
						t_htmP  = '<img src="{pic}">'.format(pic = tweet['media_url'])
						print(t_htmP)
			
	if req_form.getvalue('videos_option'):
		twitter_resp = myTwitterReq.makeRequest(myLocation.lat, myLocation.long, distanceKM, maxcount, 'videos')
		#print ( apiRequest.json.dumps(twitter_resp, indent=4, sort_keys=True))
		for tweets in twitter_resp['statuses']:
			if 'media' not in tweets['entities']:
				continue
			else:
				for tweet in tweets['entities']['media']:
					if 'expanded_url' not in tweet:
						continue
					else:
						t_htmV = tweet['expanded_url'] 
						#t_htmV  = '<iframe>width="440" height="290" src="{vid}" frameborder="1"/></iframe><br>'.format(vid = tweet['expanded_url'])
						print(t_htmV)
						
	if req_form.getvalue('tweets_option'):
		twitter_resp = myTwitterReq.makeRequest(myLocation.lat, myLocation.long, distanceKM, maxcount)
		for tweets in twitter_resp['statuses']:
			print (tweets['text'].encode('ascii', 'ignore'))

print ('</p> </body> </html>')





