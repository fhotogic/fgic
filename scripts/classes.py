import pymysql as myDB
#Location class. Location will be used by various classes such as
#Pictures , video etc. via "Media" class.
class Location:
	'class for location information.'
	'With poi name as the name of a prominant location.'
	'Such as golden gate bridge'
	def __init__(self, lat = 0, long = 0, poiname = 0, locname = 0):
		self.lat = lat
		self.long = long
		self.poiName = poiname
		self.locationName = locname
	
	def getLocation(self):
		return ( self.lat, self.long, self.PoiName, self.locationName)
		

#Parent class for all the media		
class Media:
	'Parent class for pictures, tweets and videos'
	def __init__(self):
		self.name = ""
		self.location = Location()
		
				
class Picture(Media):
	'Class for picture information.'
	'Filetype is file extension.'
	'Quality is resolution.'
	def __init__(self):
		self.quality = ""
		self.filetype = ""
	
	def getPicInfo(self):
		return ( self.name, self.location, self.quality, self.filetype )
		
	def setPicInfo(self, qual, type, name, location):
		self.quality = qual
		self.filetype = type
		self.name = name
		self.location = location
		
		
		
class Video(Media):
	def __init__(self):
		self.length = ""
		self.filetype = ""
	
	def getVideoInfo(self):
		return ( self.name, self.location, self.length, self.filetype )
	
	def setVideoInfo(self, len, type, name, location):
		self.length = len
		self.filetype = type
		self.name = name
		self.location = location
		

		
class User:
	'user information for authentication and tracking'
	def __init__(self, fname = "", lname = "", email = "", passw = ""):
		self.first_name = fname
		self.last_name = lname
		self.email = email
		self.password = passw
	
	def getUserInfo(self):
		return ( self.first_name, self.lasy_name, self.email, self.password )
		

		
#Contains Databse access details and also user validation mechanism		
class DBaccess:
	'database connection class'
	def __init__(self):
		self.connectionString = "localhost"
		self.port = 3306
		self.username = "neetu"
		self.password = "Neetu123"
		self.dB = "neetu" 
		self.user = User()
		self.conn = myDB.connect(host = self.connectionString, port = self.port, user = self.username, passwd = self.password, db = self.dB, autocommit = True)
		self.cur = self.conn.cursor()
		
	def createConn(self):
		return self.cur
		
	def validateUser(self, dbConnection, user):
		self.query = "SELECT password FROM users WHERE email = '" + user.email + "'"
		dbConnection.execute(self.query)
		for row in dbConnection:
			if row[0] == user.password:
				return True
			else:
				return False			
	
	def dbExecuteQuery(self, dbConnection, query):
		pass
	
	def deleteUser(self, dbConnection, user):
		self.query = "DELETE FROM users WHERE email = '" + user.email +"' AND password = '" + user.password + "'"
		dbConnection.execute(self.query)
				
	def addUser(self, dbConnection, user):
		self.query = "INSERT INTO users (name,email,password) VALUES (%s,%s,%s)"
		ret = dbConnection.execute(self.query, (user.first_name + " " + user.last_name, user.email, user.password))
		return ret
	
	
#To manage http sessions		
class SessionManager:
	'http user login session management'
	def __init__(self, id = 0, user = User()):
		self.sessionID = id
		self.user = user
	
	def getSessionInfo(self):
		return(self.sessionID, self.user)
		

#Not used currently. Logic to be implemented.		
class UserActivity:
#	'collect and save user activity'
	def __init__(self, activityType = 0):
		self.activity = activityType
	
	
	
	